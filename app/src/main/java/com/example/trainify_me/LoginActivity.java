package com.example.trainify_me;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {
    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        auth = FirebaseAuth.getInstance();
        CheckBox rememberMeBox = findViewById(R.id.Remember_me);
        rememberMeBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked()){
                    SharedPreferences sharedPreferences = getSharedPreferences("rememberMeCheckBox", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("rememberUser", "true");
                    editor.apply();
                    Toast.makeText(LoginActivity.this, "user saved", Toast.LENGTH_SHORT).show();
                }

                else if (!compoundButton.isChecked()){
                    SharedPreferences sharedPreferences = getSharedPreferences("rememberMeCheckBox", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("rememberUser", "false");
                    editor.apply();
                    Toast.makeText(LoginActivity.this, "user saved", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public void SubmitLoginButton(View view) {
        String password = ((EditText) findViewById(R.id.Password)).getText().toString().trim();
        String email = ((EditText) findViewById(R.id.Email)).getText().toString().trim();
        FirebaseUser user = auth.getCurrentUser();

        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Missing credentials", Toast.LENGTH_SHORT).show();
        } else {
            auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(task -> {
                        if (!task.isSuccessful()) {
                            Toast.makeText(this, "Email or password not exist", Toast.LENGTH_SHORT).show();
                        }

                        else {
                            Toast.makeText(this, "welcome user " + user.getDisplayName(), Toast.LENGTH_SHORT).show();

                            startActivity(new Intent(LoginActivity.this, TrainActivity.class));
                            finish();
                        }
                    });
        }
    }
    public void resetPassword(View view) {
        startActivity(new Intent(LoginActivity.this, ForgotPassword.class));
        finish();
    }

    @Override
    public void onBackPressed(){
        startActivity( new Intent(this, MainActivity.class) );
        finish();
    }
}