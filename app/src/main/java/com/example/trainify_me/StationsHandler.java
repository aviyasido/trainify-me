package com.example.trainify_me;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class StationsHandler {
    public static final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
            .getReferenceFromUrl("https://trainify-me-default-rtdb.firebaseio.com/").child("stations");

    public static void setStations(){
        Map<String, Stations> stationsMap = new HashMap<>();

        stationsMap.put("0", new Stations("Ofakim", "אופקים", "POINT(34.6342446321416 31.3216324597401)"));
        stationsMap.put("1", new Stations("Achihud", "אחיהוד", "POINT(35.1734330200682 32.9116024790683)"));
        stationsMap.put("2", new Stations("Ashdod Ad halom", "אשדוד עד הלום", "POINT(34.6661928184092 31.7739455452703)"));
        stationsMap.put("3", new Stations("Ashkelon", "אשקלון", "POINT(34.6044906769883 31.6759320914563)"));
        stationsMap.put("4", new Stations("Be'er yaakov", "באר יעקב", "POINT(34.830564979899 31.932466890081)"));
        stationsMap.put("5", new Stations("Be'er sheva center", "באר שבע - מרכז", "POINT(34.7989090105577 31.242007300062)"));
        stationsMap.put("6", new Stations("Be'er sheva North Haoniversita", "באר שבע - צפון האוניברסיטה", "POINT(34.8094560410801 31.2619753966637)"));
        stationsMap.put("7", new Stations("Beit yehoshea", "בית יהושע", "POINT(34.8602367189849 32.2626015651145)"));
        stationsMap.put("8", new Stations("Beit shean", "בית שאן", "POINT(35.4884093519709 32.51484060906)"));
        stationsMap.put("9", new Stations("Beit shemesh", "בית שמש", "POINT(34.9893698281397 31.7578255045203)"));
        stationsMap.put("10", new Stations("Bnei brak", "בני ברק", "POINT(34.8305411747051 32.1028989911551)"));
        stationsMap.put("11", new Stations("Binyamina", "בנימינה", "POINT(34.9496349066016 32.5145585562963)"));
        stationsMap.put("12", new Stations("Bat yam - Hakomemiut", "בת ים - הקוממיות", "POINT(34.7593027102761 32.0008568768613)"));
        stationsMap.put("13", new Stations("Bat yam - Yoseftal", "בת ים - יוספטל", "POINT(34.7620221836622 32.0149365284122)"));
        stationsMap.put("14", new Stations("Dimona", "דימונה", "POINT(35.0118347392278 31.0689330686615)"));
        stationsMap.put("15", new Stations("Hod hasharon - Sokolov", "הוד השרון - סוקולוב", "POINT(34.9020059343032 32.1700206651523)"));
        stationsMap.put("16", new Stations("Hertzelia", "הרצליה", "POINT(34.8175974813538 32.1639989557637)"));
        stationsMap.put("17", new Stations("Hadera - west", "חדרה - מערב", "POINT(34.8993283812025 32.4383901731264)"));
        stationsMap.put("18", new Stations("Holon Wolfson", "חולון וולפסון", "POINT(34.7596909000004 32.0353104823927)"));
        stationsMap.put("19", new Stations("Hutzot hamifratz", "חוצות המפרץ", "POINT(35.0543432869546 32.8094870193685)"));
        stationsMap.put("20", new Stations("Heifa - Bat galim", "חיפה - בת גלים", "POINT(34.9817501571633 32.8303711662665)"));
        stationsMap.put("21", new Stations("Heifa - Hof hacarmel", "חיפה - חוף הכרמל", "POINT(34.9573368985797 32.7935292323997)"));
        stationsMap.put("22", new Stations("Heifa - Hashmona center", "חיפה - מרכז השמונה", "POINT(34.9973533580942 32.8223251994612)"));
        stationsMap.put("23", new Stations("Yavne - East", "יבנה - מזרח", "POINT(34.7438823688453 31.8613746767201)"));
        stationsMap.put("24", new Stations("Yavne - West", "יבנה - מערב", "POINT(34.7312399691522 31.8911952114772)"));
        stationsMap.put("25", new Stations("Yokneam - kfar yehoshea", "יוקנעם - כפר יהושע", "POINT(35.1256851838206 32.6809216386319)"));
        stationsMap.put("26", new Stations("Jerusalem - Itzhak navon", "ירושלים - יצחק נבון", "POINT(35.2027616556088 31.7881197034415)"));
        stationsMap.put("27", new Stations("Kfar habad", "כפר חבד", "POINT(34.8533302933951 31.9929053698826)"));
        stationsMap.put("28", new Stations("Kfar sava - Nordo", "כפר סבא - נורדאו", "POINT(34.9167915810366 32.167471958345)"));
        stationsMap.put("29", new Stations("Karmiel", "כרמיאל", "POINT(35.2971686942839 32.9243265192769)"));
        stationsMap.put("30", new Stations("Lehavim - Rahat", "להבים - רהט", "POINT(34.7979891868116 31.3698138536874)"));
        stationsMap.put("31", new Stations("Lod", "לוד", "POINT(34.8780379667761 31.9476528352155)"));
        stationsMap.put("32", new Stations("Lod - Ganei aviv", "לוד - גני אביב", "POINT(34.8787050122924 31.9670463350968)"));
        stationsMap.put("33", new Stations("Migdal haeemek - kfar baruch", "מגדל העמק - כפר ברוך", "POINT(35.2094461797839 32.6472040655567)"));
        stationsMap.put("34", new Stations("Modi'in center", "מודיעין - מרכז", "POINT(35.0057255192167 31.9010426180689)"));
        stationsMap.put("35", new Stations("Mazkeret batia", "מזכרת בתיה", "POINT(34.856301355104 31.8421399336336)"));
        stationsMap.put("36", new Stations("Merkazit hamifratz", "מרכזית המפרץ", "POINT(35.0331627442534 32.792827762442)"));
        stationsMap.put("37", new Stations("Nahariya", "נהריה", "POINT(35.0988118678889 33.0049544946599)"));
        stationsMap.put("38", new Stations("Nemal hateufa Ben guion", "נמל התעופה בן גוריון", "POINT(34.8704650434313 32.0003770662029)"));
        stationsMap.put("39", new Stations("Netivot", "נתיבות", "POINT(34.5716793354715 31.4107281775782)"));
        stationsMap.put("40", new Stations("Netanya", "נתניה", "POINT(34.8693239405184 32.3199496699949)"));
        stationsMap.put("41", new Stations("Netanya - Sapir", "נתניה - ספיר", "POINT(34.8655043649459 32.2796705052529)"));
        stationsMap.put("42", new Stations("Aco", "עכו", "POINT(35.0830646552225 32.9281893517899)"));
        stationsMap.put("43", new Stations("Afula", "עפולה", "POINT(35.2953563396761 32.6218344902695)"));
        stationsMap.put("44", new Stations("Athlit", "עתלית", "POINT(34.940192801411 32.6927876268636)"));
        stationsMap.put("45", new Stations("Paatei Modi'in", "פאתי מודיעין", "POINT(34.9606666453725 31.893031490879)"));
        stationsMap.put("46", new Stations("Patah tikva - Kiryat arie", "פתח תקווה - קריית אריה", "POINT(34.8628336398486 32.1062469324708)"));
        stationsMap.put("47", new Stations("Petah tikva - Sgula", "פתח תקווה סגולה", "POINT(34.9010315 32.1118957330244)"));
        stationsMap.put("48", new Stations("Tzomet holon", "צומת חולון", "POINT(34.7764173865468 32.0370424774)"));
        stationsMap.put("49", new Stations("keisaria - Pardes hana", "קיסריה - פרדס חנה", "POINT(34.954162881117 32.4852678811964)"));
        stationsMap.put("50", new Stations("Kiryat gat", "קריית גת", "POINT(34.7779343146355 31.6031502744884)"));
        stationsMap.put("51", new Stations("Kiryat haim", "קריית חיים", "POINT(35.0641467279587 32.8248689508504)"));
        stationsMap.put("52", new Stations("Kiryat motzkin", "קריית מוצקין", "POINT(35.0699195087366 32.8329628439018)"));
        stationsMap.put("53", new Stations("Kiryat malachi - Yoav", "קריית מלאכי - יואב", "POINT(34.822503586562 31.7469172326455)"));
        stationsMap.put("54", new Stations("Rosh haayeen - North", "ראש העין צפון", "POINT(34.934461830854 32.1206449342585)"));
        stationsMap.put("55", new Stations("Rishon letzion - Harishonim", "ראשון לציון - הראשונים", "POINT(34.8030669419233 31.9488029941683)"));
        stationsMap.put("56", new Stations("Rishon letzion - Moshe Dayan", "ראשון לציון - משה דיין", "POINT(34.7572904467474 31.9877039051528)"));
        stationsMap.put("57", new Stations("Rehovot", "רחובות", "POINT(34.8069143311506 31.9091105033396)"));
        stationsMap.put("58", new Stations("Ramla", "רמלה", "POINT(34.8767246648476 31.929230319767)"));
        stationsMap.put("59", new Stations("Raanana south", "רעננה דרום", "POINT(34.8866086764112 32.1726539354397)"));
        stationsMap.put("60", new Stations("Raanana west", "רעננה מערב", "POINT(34.8506565340938 32.180155910706)"));
        stationsMap.put("61", new Stations("Sderot", "שדרות", "POINT(34.5854272575706 31.5157760311602)"));
        stationsMap.put("62", new Stations("Tel Aviv - Haoniversita", "תל אביב - האוניברסיטה", "POINT(34.8046132138631 32.1037104549697)"));
        stationsMap.put("63", new Stations("Tel Aviv - Haagana", "תל אביב - ההגנה", "POINT(34.7848034790735 32.0541451053854)"));
        stationsMap.put("64", new Stations("Tel Aviv - Hashalom", "תל אביב - השלום", "POINT(34.7931591146097 32.0735594000005)"));
        stationsMap.put("65", new Stations("Tel Aviv - Savidor center", "תל אביב - סבידור מרכז", "POINT(34.7983117000006 32.0842437000005)"));

        databaseReference.setValue(stationsMap);
    }

    public static class Stations {
        public String englishName;
        public String hebrewName;
        public String coordinates;

        public Stations(String englishName, String hebrewName, String coordinates) {
            this.englishName = englishName;
            this.hebrewName = hebrewName;
            this.coordinates = coordinates;
        }
    }
}
