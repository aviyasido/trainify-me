package com.example.trainify_me;

import android.app.Activity;
import android.widget.Toast;

import com.location.aravind.getlocation.GeoLocator;

import net.sf.geographiclib.Geodesic;
import net.sf.geographiclib.GeodesicData;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.WKTReader;

public class TimerHandler implements Runnable {
    private final String destination;
    private final Activity activity;
    private static double distance;
    private double calculateDistance(Coordinate coord1, Coordinate coord2) {
        GeodesicData g = Geodesic.WGS84.Inverse(coord1.y, coord1.x, coord2.y, coord2.x);
        return g.s12;  // distance in metres
    }
    public TimerHandler(String destination, Activity activity) {
        this.destination = destination;
        this.activity = activity;
    }

    @Override
    public void run() {
        GeoLocator locator = new GeoLocator(activity.getApplicationContext(),activity);
        double latitude = locator.getLattitude();
        double longitude = locator.getLongitude();

        String locationWKT = "POINT(" + longitude + " " + latitude + ")";
        try {
            Geometry myLocation = new WKTReader().read(locationWKT);
            Geometry destLocation = new WKTReader().read(destination);

            Coordinate myLocationCoordinate = myLocation.getCoordinate();
            Coordinate destLocationCoordinate = destLocation.getCoordinate();

            distance = calculateDistance(myLocationCoordinate, destLocationCoordinate);

//            display background thread
            activity.runOnUiThread(() -> {
                Toast.makeText(activity, "distance from destination is " + Math.round(distance) + ("m"), Toast.LENGTH_SHORT).show();
            });

            int x = 7;
        }

        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
    }

    public boolean isStation(){
        return distance <= 300;
    }
    public static double getDistance(){
        return distance;
    }
}
