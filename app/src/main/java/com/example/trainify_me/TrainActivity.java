package com.example.trainify_me;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class TrainActivity extends AppCompatActivity {
    public static final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
            .getReferenceFromUrl("https://trainify-me-default-rtdb.firebaseio.com/").child("stations");
    private final List<String> list = new ArrayList<>();
    private Context context = this;
    private static final String id_arrived = "trainify-me-notify-1";
    private static final String id_distance = "trainify-me-notify-2";
    private static final int PERMISSION_REQUEST_CODE = 111;
    String destinationLocation = "";
    private FusedLocationProviderClient fusedLocationClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        requestPermission();

        databaseReference.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DataSnapshot dataSnapshot = task.getResult();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    String value = data.child("hebrewName").getValue(String.class);
                    list.add(value);
                    Log.d("TAG", "Value: " + value);
                }

                startSpinner();
            } else {
                Log.w("TAG", "Failed to read value.", task.getException());
            }
        });
    }

    private void startSpinner() {
        Spinner stationChoice = findViewById(R.id.searchable_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
        stationChoice.setAdapter(adapter);
        stationChoice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                databaseReference.get().addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DataSnapshot dataSnapshot = task.getResult();
                        destinationLocation = dataSnapshot.child(String.valueOf(position)).child("coordinates").getValue(String.class);
                        Log.d("TAG", "the coords are " + destinationLocation);
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing
            }
        });
    }

    public void monitorButton(View view) {
        if (destinationLocation.equals("")) {
            Toast.makeText(this, "needs to choose station destination first", Toast.LENGTH_SHORT).show();
        } else {
            getLastLocation();
            Monitor monitor = new Monitor(this, destinationLocation);
            Button button = (Button) view;
            button.setEnabled(false);

            monitor.monitorButton();
            if (monitor.getDistance() > 300) {
                distanceFromStation(monitor.getDistance());
            } else {
                setNotification();
            }
        }
    }

    public void logOutClickImageView(View view) {
        SharedPreferences sharedPreferences = getSharedPreferences("rememberMeCheckBox", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("rememberUser", "false");
        editor.apply();
        startActivity(new Intent(TrainActivity.this, MainActivity.class));
        finish();
    }


    private void setNotification() {
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        NotificationChannel notificationChannel = new NotificationChannel(id_arrived, "arrive", NotificationManager.IMPORTANCE_HIGH);
        notificationChannel.enableVibration(true);
        notificationChannel.setVibrationPattern(new long[]{100, 1000, 200, 340});

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, id_arrived)
                .setContentTitle("We trainify you that")
                .setContentText("you've arrived to your destination")
                .setSmallIcon(R.drawable.train_icon);

        manager.createNotificationChannel(notificationChannel);
        manager.notify(1, builder.build());
    }

    private void distanceFromStation(double distance) {
        NotificationChannel notificationChannel = new NotificationChannel(id_distance, "distance", NotificationManager.IMPORTANCE_DEFAULT);
        NotificationManager manager = getSystemService(NotificationManager.class);
        manager.createNotificationChannel(notificationChannel);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, id_distance)
                .setSmallIcon(R.drawable.train_icon)
                .setContentTitle("We trainify you that")
                .setContentText("Your distance from destination station is " + distance);

        manager.notify(1, builder.build());
    }

    private void requestPermission() {
        while (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.POST_NOTIFICATIONS}, PERMISSION_REQUEST_CODE);
        }
    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        Toast.makeText(TrainActivity.this, "inside onSuccess", Toast.LENGTH_SHORT).show();
                        if (location != null) {
                            Toast.makeText(TrainActivity.this, "Lat = " + location.getLatitude() + " Lon = " + location.getLongitude(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
