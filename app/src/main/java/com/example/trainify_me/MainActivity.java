package com.example.trainify_me;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set train stations
        StationsHandler.setStations();

        //insert directly to trains if already signed up
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String name = user.getDisplayName();
        SharedPreferences sharedPreferences = getSharedPreferences("rememberMeCheckBox", MODE_PRIVATE);
        String checkBox = sharedPreferences.getString("rememberUser", "");
        if (checkBox.equals("true")) {
            Toast.makeText(this, "welcome user " + name, Toast.LENGTH_SHORT).show();
            startActivity(new Intent(MainActivity.this, TrainActivity.class));
        } else if (checkBox.equals("false")) {
            Toast.makeText(this, "needs to sign in", Toast.LENGTH_SHORT).show();
        }
    }

    public void signupClickButton(View view) {
        startActivity(new Intent(MainActivity.this, SignupActivity.class));
        finish();
    }

    public void logInClickButton(View view) {
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        finish();
    }
}
