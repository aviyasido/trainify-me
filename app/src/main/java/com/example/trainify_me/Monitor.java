package com.example.trainify_me;

import android.app.Activity;
import android.widget.Toast;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Monitor {
    private final Activity activity;
    private final String destinationLocation;
    private double distance;

    public Monitor(Activity activity, String destinationLocation) {
        this.activity = activity;
        this.destinationLocation = destinationLocation;
    }

    public void monitorButton() {
        Toast.makeText(activity, "start monitoring", Toast.LENGTH_SHORT).show();
        ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
        TimerHandler runnable = new TimerHandler(destinationLocation, activity);
        runnable.run();

        if (!runnable.isStation()) {
            service.scheduleAtFixedRate(new TimerHandler(destinationLocation, activity), 0, 10, TimeUnit.SECONDS);
            distance = TimerHandler.getDistance();
        }
    }

    public double getDistance(){
        return distance;
    }
}
