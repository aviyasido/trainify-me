package com.example.trainify_me;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class SignupActivity extends AppCompatActivity {
    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        auth = FirebaseAuth.getInstance();
        CheckBox rememberMeBox = findViewById(R.id.Remember_me);
        rememberMeBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked()){
                    SharedPreferences sharedPreferences = getSharedPreferences("rememberMeCheckBox", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("rememberUser", "true");
                    editor.apply();
                    Toast.makeText(SignupActivity.this, "user saved", Toast.LENGTH_SHORT).show();
                }

                else if (!compoundButton.isChecked()){
                    SharedPreferences sharedPreferences = getSharedPreferences("rememberMeCheckBox", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("rememberUser", "false");
                    editor.apply();
                    Toast.makeText(SignupActivity.this, "user saved", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void registerUsers(String email, String password, String name) {
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                FirebaseUser user = auth.getCurrentUser();
                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                        .setDisplayName(name)
                        .build();

                user.updateProfile(profileUpdates)
                        .addOnCompleteListener(profileTask -> {
                            if (profileTask.isSuccessful()) {
                                Toast.makeText(SignupActivity.this, "User signed up successfully", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(SignupActivity.this, TrainActivity.class));
                                finish();
                            }});
            } else {
                Toast.makeText(SignupActivity.this, "failed to sign up", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void SubmitSignInButton(View view) {
        String name = ((EditText)findViewById(R.id.Name)).getText().toString().trim();
        String email = ((EditText)findViewById(R.id.Email)).getText().toString().trim();
        String password = ((EditText)findViewById(R.id.Password)).getText().toString().trim();

        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(email) || TextUtils.isEmpty(password)){
            Toast.makeText(SignupActivity.this, "Missing credentials", Toast.LENGTH_SHORT).show();
        } else if (password.length() < 6) {
            Toast.makeText(SignupActivity.this, "Password too short", Toast.LENGTH_SHORT).show();
        } else {
            registerUsers(email, password, name);
        }
    }

    @Override
    public void onBackPressed(){
        startActivity( new Intent(this, MainActivity.class) );
        finish();
    }
}